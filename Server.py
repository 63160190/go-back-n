import socket
import random
import time


def simulate_packet_loss(loss_rate):
    return random.uniform(0.1, 0.9) >= loss_rate

def main():
    port = 1866
    chunk_size = 100
    loss_rate = 0.5
    Rn = 0  

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind(('127.0.0.1', port))

    print(f"Server is listening on port {port} with chunk size {chunk_size} and loss rate {loss_rate}")

    received_data = b""  

    while True:
        data, client_address = server_socket.recvfrom(chunk_size)
        seq_no = int.from_bytes(data[4:8], byteorder='big')
        if simulate_packet_loss(loss_rate) and Rn == seq_no:
            server_socket.sendto(data, client_address)  
            print(f"Received {len(data)} bytes from {client_address}") 
            received_data += data[12:]  
            print("Result: ", received_data)
            Rn += 1 
        else:
            
            print(f"Packet loss, sequence number {len(data)} bytes from {client_address}")

if True:
    main()
