import socket
import sys
import time

class GBNStruct:
    def __init__(self, seq_no, ack_no, data_size, data):
        self.seq_no = seq_no
        self.ack_no = ack_no
        self.data_size = data_size
        self.data = data
        self.is_acknowledged = False
        self.last_sent_time = None

    def serialize(self):
        seq_no_bytes = self.seq_no.to_bytes(4, byteorder='big')
        ack_no_bytes = self.ack_no.to_bytes(4, byteorder='big')
        data_size_bytes = self.data_size.to_bytes(4, byteorder='big')
        serialized_data = seq_no_bytes + ack_no_bytes + data_size_bytes + self.data
        return serialized_data

    @staticmethod
    def deserialize(data):
        seq_no = int.from_bytes(data[0:4], byteorder='big')
        ack_no = int.from_bytes(data[4:8], byteorder='big')
        data_size = int.from_bytes(data[8:12], byteorder='big')
        packet_data = data[12:]
        return GBNStruct(seq_no, ack_no, data_size, packet_data)

def main():
    server_ip = "127.0.0.1"
    server_port = 1866
    chunk_size = 2
    window_size = 4

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    client_socket.settimeout(0.1)

    packets = []
    base = 0
    next_seq_num = 0
    timeout = 2
    trys = 10

    with open('filetext.txt', 'rb') as file:
        while True:
            data = file.read(chunk_size)
            if not data:
                break
            packet = GBNStruct(next_seq_num, next_seq_num, len(data), data)
            packet.last_sent_time = time.time()
            packets.append(packet)
            next_seq_num += 1

    start_time = time.time()
    print(f"Total Packet: {len(packets)} \n")
    while base < len(packets):
        for i in range(base, min(base + window_size, len(packets))):
            if i == base or (i < next_seq_num and not packets[i].is_acknowledged):
                if packets[i].is_acknowledged:
                    client_socket.sendto(packets[i].serialize(), (server_ip, server_port))
                    packets[i].last_sent_time = time.time()
                elif time.time() - packets[i].last_sent_time >= timeout:
                    print(f"Sending packet {i}")
                    client_socket.sendto(packets[i].serialize(), (server_ip, server_port))
                    packets[i].last_sent_time = time.time()
                    if i == window_size+base-1 or (len(packets)-base < window_size) and  i==len(packets)-1:
                        trys-=1
                        print(f"Trys more {trys} \n")
                    if trys == 0:
                        print("No Response: Interrupted system call")
                        return
        try:
            data, server_address = client_socket.recvfrom(1024)
            ack_packet = GBNStruct.deserialize(data)
            if ack_packet is not None and hasattr(ack_packet, 'ack_no'):
                ack_no = ack_packet.ack_no
                if packets[ack_no].is_acknowledged is False: 
                    print(f"\nReceived ACK for packet {ack_no} {packets[ack_no].data}")
                    packets[ack_no].is_acknowledged = True
                    trys = 10
                    if ack_no == base:
                        while base < len(packets) and packets[base].is_acknowledged:
                            base += 1
        except socket.timeout:
            pass

    end_time = time.time()
    total_time = end_time - start_time
    throughput = len(packets) / total_time

    print(f"\nFile transfer completed in {total_time:.2f} seconds.")
    print(f"Throughput: {throughput:.2f} packets per second")
    client_socket.close()

if True:
    main()